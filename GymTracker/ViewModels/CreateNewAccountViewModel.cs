﻿using System;
using System.Threading.Tasks;
using GymTracker.Models;
using GymTracker.Services;

namespace GymTracker.ViewModels
{
    public class CreateNewAccountViewModel : BaseViewModel
    {

        public AccountInfo User { get;} = new AccountInfo();
        public string ErrorPrompt { get; set; } = "";

        public CreateNewAccountViewModel()
        {
        }

        public bool ValidateInformation()
        {
            ErrorPrompt = User.GetInformationInvalidReason();

            return ErrorPrompt == "";
        }

        

        public async Task<bool> SubmitInformation()
        {
            await Task.Delay(1000);
            Task<bool> success = Task.FromResult(false);
            if(ValidateInformation())
                success = WebServices.CreateNewAccount(User);
            else
            {
                User.Password = "";
                User.ConfirmPassword = "";
            }

            return await success;
        }
    }
}
