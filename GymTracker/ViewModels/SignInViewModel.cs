﻿using System;
using GymTracker.Views;
using Xamarin.Forms;
using GymTracker.Services;
using System.Threading.Tasks;

namespace GymTracker.ViewModels
{
    public class SignInViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string PromptMessage { get; set; }

        public SignInViewModel()
        {
        }

        public void Clear()
        {
            Username = "";
            Password = "";
            PromptMessage = "";
        }

        public async Task<bool> Authenticate()
        {
            bool Authenticated = await WebServices.Authenticate(Username, Password);

            if (!Authenticated)
            {
                ClearPasswordField();
                PromptMessage = "The username/password combination is incorrect";
            }
            return Authenticated;
        }

        public void ClearPasswordField()
        {
            Password = "";
        }
    }
}
