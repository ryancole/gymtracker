﻿using System;
using System.Collections.Generic;
using GymTracker.ViewModels;
using Xamarin.Forms;

namespace GymTracker.Views
{
    public partial class CreateNewAccountPage : ContentPage
    {
        CreateNewAccountViewModel viewModel;
        SignInPage PreviousPage;

        public CreateNewAccountPage(SignInPage PreviousPage)
        {
            InitializeComponent();
            BindingContext = viewModel = new CreateNewAccountViewModel();
            this.PreviousPage = PreviousPage;
        }

        async void Submit_Clicked(Object sender, EventArgs e)
        {
            View tempContent = Content;
            Content = new ActivityIndicator() { IsRunning = true };
            
            if (await viewModel.SubmitInformation())
            {
                Navigation.RemovePage(PreviousPage);
                Navigation.InsertPageBefore(new MainPage(), this);
                await Navigation.PopAsync();
            }
            else
            {
                Content = tempContent;
            }
            
        }
    }
}
