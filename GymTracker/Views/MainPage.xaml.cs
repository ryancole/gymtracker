﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace GymTracker.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        override protected void OnAppearing()
        {
            Navigation.PopAsync();
        }
    }
}
