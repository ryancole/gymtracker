﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using GymTracker.ViewModels;

namespace GymTracker.Views
{
    public partial class SignInPage : ContentPage
    {
        SignInViewModel viewModel;

        public SignInPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SignInViewModel();
        }


        async void OnSignInClicked(System.Object sender, System.EventArgs e)
        {
            if (await viewModel.Authenticate())
            {
                NavigateToMainPage();
            }
        }

        async void CreateNew_Clicked(System.Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new CreateNewAccountPage(this));
        }

        async void NavigateToMainPage()
        {
            Navigation.InsertPageBefore(new MainPage(), this);
            await Navigation.PopAsync(false);
        }

        protected override void OnDisappearing()
        {
            viewModel.Clear();

        }
    }
}
