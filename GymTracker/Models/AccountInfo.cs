﻿using System;
namespace GymTracker.Models
{
    public class AccountInfo
    {
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string EmailAddress { get; set; } = "";
        public string Password { get; set; } = "";
        public string ConfirmPassword { get; set; } = "";


        public string GetInformationInvalidReason()
        {
            string reason = "";
            if (FirstName == "" || LastName == "")
                reason = "Must supply name field";
            else if (!ValidEmail(EmailAddress))
                reason = "Must supply valid email";
            else if (!ValidPassword(Password))
                reason = "Must supply valid password";
            else if (Password != ConfirmPassword)
                reason = "Passwords do not match!";

            return reason;
        }

        private bool ValidEmail(string email)
        {
            //actually validate email, also check that it's not taken
            return email != "";
        }

        private bool ValidPassword(string password)
        {
            //actually validate password
            return password != "";
        }
    }
}
